#!/usr/bin/env bash

#http://fedoraproject.org/wiki/Packaging%3aUsersAndGroups

GROUPNAME="kube"
USERNAME="kube"
HOMEDIR="/"

getent group ${GROUPNAME} >/dev/null || groupadd -r ${GROUPNAME}
getent passwd ${USERNAME} >/dev/null || \
useradd -r -g ${GROUPNAME} -d ${HOMEDIR} -s /sbin/nologin \
    -c "Kubernetes user" ${USERNAME}
